import "./ProductsSection.scss";
import ProductsList from '../ProductsList';

function ProductsSection() {

        return (
            <section className='products'>

                <div className='container'>

                    <div className='products__content'>
                        <h2>Albums Currently on sale</h2>

                        <ProductsList />
                    </div>
                </div>

            </section>
        );
    }

export default ProductsSection;